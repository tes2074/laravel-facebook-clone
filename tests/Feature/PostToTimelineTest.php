<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class PostToTimelineTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        Storage::fake('public');
    }

    /**
     * @test
     */
    public function a_user_can_post_a_text_post()
    {
        $this->withoutExceptionHandling();
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $body_text = 'test body';

        $response = $this->post('/api/posts', [
            'body' => $body_text
        ]);

        $post = Post::first();

        $this->assertEquals($user->id, $post->user_id);
        $this->assertEquals($body_text, $post->body);
        $response->assertStatus(201)
            ->assertJson([
                'data' => [
                    'type' => 'posts',
                    'post_id' => $post->id,
                    'posted_by' => [
                        'data' => [
                            'attributes' => [
                                'name' => $user->name
                            ]
                        ]
                    ],
                    'attributes' => [
                        'body' => $body_text
                    ]
                ],
                'links' => [
                    'self' => url('/posts/' . $post->id)
                ]
            ]);
    }

    /**
     * @test
     */
    public function a_user_can_post_a_text_post_with_an_image()
    {
        $this->withoutExceptionHandling();
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $body_text = 'test body';
        $file = UploadedFile::fake()->image('post-image.jpg');

        $response = $this->post('/api/posts', [
            'body' => $body_text,
            'image' => $file,
            'width' => 300,
            'height' => 200
        ])->assertStatus(201);

        Storage::disk('public')->assertExists('post-images/' . $file->hashName());
        $response->assertStatus(201)
            ->assertJson([
                'data' => [
                    'attributes' => [
                        'body' => $body_text,
                        'image' => url('post-images/' . $file->hashName())
                    ]
                ]
            ]);
    }
}
