<?php

namespace Tests\Feature;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostCommentsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function a_user_can_comment_on_a_post()
    {
//        $this->withoutExceptionHandling();
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $post = Post::factory()->create([
            'id' => 123
        ]);
        $bodyText = 'Comment text';

        $response = $this->post('/api/posts/' . $post->id . '/comment', [
            'body' => $bodyText
        ])
            ->assertStatus(200);
        $this->assertCount(1, Comment::all());

        $comment = Comment::first();
        $this->assertEquals($user->id, $comment->user_id);
        $this->assertEquals($post->id, $comment->post_id);
        $this->assertEquals($bodyText, $comment->body);

        $response->assertJson([
            'data' => [
                [
                    'data' => [
                        'type' => 'comments',
                        'comment_id' => 1,
                        'attributes' => [
                            'body' => $bodyText,
                            'commented_by' => [
                                'data' => [
                                    'type' => 'users',
                                    'user_id' => $user->id,
                                    'attributes' => [
                                        'name' => $user->name,
                                    ]
                                ]
                            ],
                            'commented_at' => $comment->created_at->diffForHumans(),
                        ]
                    ],
                    'links' => [
                        'self' => url('/posts/123'),
                    ]
                ]
            ],
            'comment_count' => 1,
            'links' => [
                'self' => url('/posts'),
            ]
        ]);
    }

    /**
     * @test
     */
    public function a_body_is_required_to_leave_a_comment_on_a_post() {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $post = Post::factory()->create([
            'id' => 123
        ]);

        $response = $this->post('/api/posts/' . $post->id . '/comment', [
            'body' => ''
        ]);
        $response->assertStatus(422);

        $responseArray = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('body', $responseArray['errors']['meta']);
    }

    /**
     * @test
     */
    public function posts_are_returned_with_comments()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $post = Post::factory()->create([
            'id' => 123,
            'user_id' => $user->id
        ]);
        $bodyText = 'Comment text';

        $this->post('/api/posts/' . $post->id . '/comment', [
            'body' => $bodyText
        ])
            ->assertStatus(200);

        $comment = Comment::first();

        $response = $this->get('/api/posts')
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    [
                        'data' => [
                            'type' => 'posts',
                            'attributes' => [
                                'comments' => [
                                    'data' => [
                                        [
                                            'data' => [
                                                'type' => 'comments',
                                                'comment_id' => 1,
                                                'attributes' => [
                                                    'body' => $bodyText,
                                                    'commented_by' => [
                                                        'data' => [
                                                            'type' => 'users',
                                                            'user_id' => $user->id,
                                                            'attributes' => [
                                                                'name' => $user->name,
                                                            ]
                                                        ]
                                                    ],
                                                    'commented_at' => $comment->created_at->diffForHumans(),
                                                ]
                                            ],
                                            'links' => [
                                                'self' => url('/posts/123'),
                                            ]
                                        ]
                                    ],
                                    'comment_count' => 1
                                ]
                            ]
                        ]
                    ]
                ]
            ]);
    }
}
