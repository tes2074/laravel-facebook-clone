<?php

namespace Tests\Feature;

use App\Models\Friend;
use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RetrievePostsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_retrieve_post()
    {
        $this->withoutExceptionHandling();
        $user = User::factory()->create();
        $anotherUser = User::factory()->create();
        $posts = Post::factory()->count(2)->create(['user_id' => $anotherUser->id]);
        $this->actingAs($user, 'api');
        $friendRequest = Friend::create([
            'user_id' => $user->id,
            'friend_id' => $anotherUser->id,
            'status' => 1,
            'confirmed_at' => now()->subDay()
        ]);

        $response = $this->get('/api/posts');

        $response->assertStatus(200)->assertJson([
            'data' => [
                [
                    'data' => [
                        'type' => 'posts',
                        'post_id' => $posts->last()->id,
                        'attributes' => [
                            'body' => $posts->last()->body,
                            'image' => url($posts->last()->image),
                            'posted_at' => $posts->last()->created_at->diffForHumans()
                        ]
                    ]
                ],
                [
                    'data' => [
                        'type' => 'posts',
                        'post_id' => $posts->first()->id,
                        'attributes' => [
                            'body' => $posts->first()->body,
                            'image' => url($posts->first()->image),
                            'posted_at' => $posts->first()->created_at->diffForHumans()
                        ]
                    ]
                ]
            ],
            'links' => [
                'self' => url('/api/posts')
            ]
        ]);
    }

    /**
     * @test
     */
    public function a_user_can_retrieve_their_only_posts()
    {
        $this->withoutExceptionHandling();
        $user = User::factory()->create();
        $posts = Post::factory()->count(2)->create();
        $this->actingAs($user, 'api');

        $response = $this->get('/api/posts');

        $response->assertStatus(200)->assertExactJson([
            'data' => [],
            'links' => [
                'self' => url('/api/posts')
            ]
        ]);
    }
}
