<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Friend extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $dates = [
      'created_at',
      'confirmed_at',
      'updated_at'
    ];

    /**
     * @param int $userId
     * @return mixed
     */
    public static function friendship(int $userId) {
        return (new static())->where(function ($query) use ($userId) {
            return $query->where([
                'user_id' => Auth::user()->id,
                'friend_id' => $userId
            ])->orWhere([
                'friend_id' => Auth::user()->id,
                'user_id' => $userId
            ]);
        })->first();
    }

    /**
     * @return mixed
     */
    public static function friendships() {
        return (new static())->whereNotNull('confirmed_at')->where(function ($query) {
            return $query->where([
                'user_id' => Auth::user()->id
            ])->orWhere([
                'friend_id' => Auth::user()->id
            ]);
        })->get();
    }
}
