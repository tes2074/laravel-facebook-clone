<?php

namespace App\Models;

use App\Scopes\ReverseScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Post extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ReverseScope());
    }

    /**
     * @return BelongsTo
     */
    public function user():BelongsTo {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsToMany
     */
    public function likes():BelongsToMany {
        return $this->belongsToMany(User::class, 'post_likes', 'post_id', 'user_id');
    }

    /**
     * @return HasMany
     */
    public function comments():HasMany {
        return $this->hasMany(Comment::class);
    }
}
