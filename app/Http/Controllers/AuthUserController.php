<?php

namespace App\Http\Controllers;

use App\Http\Resources\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthUserController extends Controller
{

    /**
     * @return User
     */
    public function show():User {
        return new User(Auth::user());
    }
}
