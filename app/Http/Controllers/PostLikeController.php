<?php

namespace App\Http\Controllers;

use App\Http\Resources\PostLikeCollection;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostLikeController extends Controller
{

    /**
     * @param Request $request
     * @param Post $post
     * @return PostLikeCollection
     */
    public function store(Request $request, Post $post):PostLikeCollection {
        $post->likes()->toggle(Auth::user());

        return new PostLikeCollection($post->likes);
    }
}
