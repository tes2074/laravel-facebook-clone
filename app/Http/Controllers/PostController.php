<?php

namespace App\Http\Controllers;

use \App\Http\Resources\Post as PostResource;
use App\Http\Resources\PostCollection;
use App\Models\Friend;
use App\Models\Post;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{

    public function index(Request $request):PostCollection {
        $friends = Friend::friendships();
        if($friends->isEmpty()) {
            $posts = $request->user()->posts;
        } else {
            $posts = Post::whereIn('user_id', [$friends->pluck('user_id'), $friends->pluck('friend_id')])->get();
        }
        return new PostCollection($posts);
    }

    /**
     * @param Request $request
     * @return PostResource
     */
    public function store(Request $request): PostResource {
        $data = $request->validate([
            'body' => '',
            'image' => '',
            'width' => '',
            'height' => ''
        ]);

        if(isset($data['image'])) {
            $image = $data['image']->store('post-images', 'public');
            Image::make($data['image'])
                ->fit($data['width'], $data['height'])
                ->save('/var/www/html/storage/app/public/post-images/' . $data['image']->hashName());
        }

        $post = $request->user()->posts()->create([
            'body' => $data['body'],
            'image' => $image ?? null
        ]);

        return new PostResource($post);
    }
}
