<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserImageResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class UserImagesController extends Controller
{

    /**
     * @param Request $request
     * @return UserImageResource
     */
    public function store(Request $request):UserImageResource {
        $data = $request->validate([
            'image' => '',
            'width' => '',
            'height' => '',
            'location' => ''
        ]);

        $image = $data['image']->store('user-images', 'public');

        Image::make($data['image'])
            ->fit($data['width'], $data['height'])
            ->save('/var/www/html/storage/app/public/user-images/' . $data['image']->hashName());

        $userImage = Auth::user()->images()->create([
            'path' => $image,
            'width' => $data['width'],
            'height' => $data['height'],
            'location' => $data['location']
        ]);

        return new UserImageResource($userImage);
    }
}
