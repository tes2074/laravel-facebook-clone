<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommentCollection;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostCommentController extends Controller
{
    /**
     * @param Request $request
     * @param Post $post
     * @return CommentCollection
     */
    public function store(Request $request, Post $post):CommentCollection {
        $data = $request->validate([
            'body' => 'required'
        ]);

        $post->comments()->create(array_merge($data, [
            'user_id' => Auth::user()->id
        ]));

        return new CommentCollection($post->comments);
    }
}
