<?php

namespace App\Http\Controllers;

use App\Http\Resources\PostCollection;
use App\Models\User;
use Illuminate\Http\Request;

class UserPostController extends Controller
{

    /**
     * @param User $user
     * @return PostCollection
     */
    public function index(User $user):PostCollection {
        return new PostCollection($user->posts);
    }
}
