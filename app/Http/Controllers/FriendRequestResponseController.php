<?php

namespace App\Http\Controllers;

use App\Exceptions\FriendRequestNotFoundException;
use App\Http\Resources\FriendResource;
use App\Models\Friend;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FriendRequestResponseController extends Controller
{

    /**
     * @param Request $request
     * @return FriendResource
     * @throws FriendRequestNotFoundException
     */
    public function store(Request $request):FriendResource {
        $data = $request->validate([
           'user_id' => 'required',
           'status' => 'required'
        ]);

        try {
            $friendRequest = Friend::where([
                'user_id' => $data['user_id'],
                'friend_id' => Auth::user()->id
            ])->firstOrFail();
        } catch(ModelNotFoundException $e) {
            throw new FriendRequestNotFoundException();
        }


        $friendRequest->update(array_merge($data, [
            'confirmed_at' => now()
        ]));

        return new FriendResource($friendRequest);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws FriendRequestNotFoundException
     */
    public function destroy(Request $request):JsonResponse {
        $data = $request->validate([
            'user_id' => 'required'
        ]);

        try {
            $friendRequest = Friend::where([
                'user_id' => $data['user_id'],
                'friend_id' => Auth::user()->id
            ])->firstOrFail();
        } catch(ModelNotFoundException $e) {
            throw new FriendRequestNotFoundException();
        }

        $friendRequest->delete();
        return response()->json([], 204);
    }
}
