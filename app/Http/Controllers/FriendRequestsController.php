<?php

namespace App\Http\Controllers;

use App\Exceptions\UserNotFoundException;
use App\Exceptions\ValidationErrorException;
use App\Http\Resources\FriendResource;
use App\Models\Friend;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class FriendRequestsController extends Controller
{

    /**
     * @param Request $request
     * @return FriendResource
     * @throws UserNotFoundException
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'friend_id' => 'required'
        ]);

        try {
            User::findOrFail($data['friend_id'])
                ->friends()->syncWithoutDetaching(Auth::user());
        } catch (ModelNotFoundException $e) {
            throw new UserNotFoundException();
        }

        return new FriendResource(Friend::where([
            'user_id' => Auth::user()->id,
            'friend_id' => $data['friend_id']
        ])->first());
    }
}
