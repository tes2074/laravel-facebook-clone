<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    /**
     * @param User $user
     * @return \App\Http\Resources\User
     */
    public function show(User $user):\App\Http\Resources\User {
        return new \App\Http\Resources\User($user);
    }
}
