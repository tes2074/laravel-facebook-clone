//our data
const state = {
    title: 'Welcome'
};

// retrieve data from module
const getters = {
    pageTitle: state => {
        return state.title;
    }
};

//all actions that we need to perform
const actions = {
    setPageTitle({commit, state}, title) {
        commit('setTitle', title);
    }
};

//things that will change state data
const mutations = {
    setTitle(state, title) {
        state.title = title + " | Facebook";
        document.title = state.title;
    }
};

export default {
    state, getters, actions, mutations
}
