//our data
const state = {
    user: null,
    userStatus: null
};

// retrieve data from module
const getters = {
    authUser: state => {
        return state.user;
    }
};

//all actions that we need to perform
const actions = {
    fetchAuthUser({commit, state}) {
        axios.get('/api/auth-user')
            .then(res => {
                commit('setAuthUser', res.data);
            }).catch(err => {
            console.log(err);
        })
    }
};

//things that will change state data
const mutations = {
    setAuthUser(state, user) {
        state.user = user;
    }
};

export default {
    state, getters, actions, mutations
}
