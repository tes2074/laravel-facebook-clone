<?php

use Illuminate\Support\Facades\Route;

\Illuminate\Support\Facades\Auth::routes();

Route::get('{any}', [App\Http\Controllers\AppController::class, 'index'])
    ->where('any', '.*')
    ->middleware('auth')
    ->name('home');
